%Author: Robin Tournemenne 19/05/17

%% HEADING
clear variables global
close all
path(path,genpath('Ressources'));

%% PROBLEM DEFINITION
%The instrument tessiture, this stands for the Bb trumpet only!
InsNotes = {{'E2','Bb3','F4','Bb4','D5','F5','Ab5','Bb5'}; 
	{'D2','Ab3','Eb4','Ab4','C5','Eb5','Gb5','Ab5'};
	{'Eb2','A3','E4','A4','Db5','E5','G5','A5'};
	{'C2','Gb3','Db4','Gb4','Bb4','Db5','E5','Gb5'}};
%The instrument note recurrence (for the MusicalMetric), this stands for the Bb trumpet only!
NoteRepartition='Ressources/NoteRepartition.mat';

%mouthpiece
GeometryMP.l=[0.0005,0.0005,0.0005,0.0005,0.0005,0.0005,0.0005,0.0005,0.0005,0.0005,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586,0.0027586];
GeometryMP.r={0.008,0.0077578,0.0074744,0.0071449,0.0067626,0.0063179,0.0057964,0.0051751,0.0044119,0.0034129,0.001825,0.0019221,0.0020191,0.0021162,0.0022133,0.0023103,0.0024074,0.0025045,0.0026016,0.0026986,0.0027957,0.0028928,0.0029898,0.0030869,0.003184,0.003281,0.0033781,0.0034752,0.0035722,0.0036693,0.0037664,0.0038634,0.0039605,0.0040576,0.0041547,0.0042517,0.0043488,0.0044459,0.0045429};
%rest of the instrument
GeometryRoB.l=[0.055,0.055,0.055,0.055,0.495,0.02,0.019,0.03,0.035,0.03,0.029,0.03,0.031,0.03,0.061,0.03,0.031,0.032,0.03,0.025,0.036,0.03,0.034,0.029,0.028];
GeometryRoB.r={[4.64e-03,5e-03],[5e-03,5.5e-03],[5.5e-03,5.7e-03],[5.7e-03,5.82e-03],[5.82e-03,5.825e-03],[5.825e-03,6.125e-03],[6.125e-03,6.1e-03],[6.1e-03,5.985e-03],[5.985e-03,6.205e-03],[6.205e-03,6.325e-03],[6.325e-03,6.65e-03],[6.65e-03,6.97e-03],[6.97e-03,7.39e-03],[7.39e-03,8.45e-03],[8.45e-03,8.68e-03],[8.68e-03,9.25e-03],[9.25e-03,9.845e-03],[9.845e-03,1.0645e-02],[1.0645e-02,1.155e-02],[1.155e-02,1.275e-02],[1.275e-02,1.4375e-02],[1.4375e-02,1.67e-02],[1.67e-02,2.25e-02],[2.25e-02,3.47e-02],[3.47e-02,6.1e-02]};

%entire Instrument
Geometry.l=[GeometryMP.l GeometryRoB.l];
Geometry.r=[GeometryMP.r GeometryRoB.r];

%variables
xs.lpos=1; %index of Geometry.l that you want to study
[xs.l{1}]=ndgrid(0:0.0005:0.006);%ndgrid takes all the values you want to test for each variable and create arrays of the corresponding dimension (1dim vector, 2dim matrix, 3dim 3dimarray, 4dim 4dimarray ...) 

%drawing the initial geometry the geometrical parameters are shown in green
DrawFromGeom(Geometry.l,Geometry.r,xs.lpos,[]);

%% PROGRAM PARAMETERs
Fing=1; %the settings are for a trumpet. If you want to modify the instrument you have to modify valueSet in ETDsCalc.m
%1=D0 (no valve pressed), 2=D1 (first valve pressed), 3=D2 (second valve pressed), 4=D23  
Notes=[2,3,5]; %don't put the reference note
Ref=4;
TransLineMethod='1DTL'; %'1DTL'  'NDTL' 
Threshold=[];%Threshold=[-20,+50]; %empty vector to avoid using it %these thresholds corresponds to acceptance limit for intonationonicity (in practise it is hard for a musician to increase the note of a pitch while it is easy to lower it)
MusicalMetric=0; %take into account the recurrence of a note in the musical pieces.

%% BORES' INHARMONICITY CALCULATION
ETDs=cell(size(xs.l{1}));
intonation=zeros(size(xs.l{1}));
for k=1:length(xs.l{1}(:)) %we use linear indexing of matlab to treat the loop easily
	
	[Z,Fpeaks]=ImpedCalc(Geometry,xs,k,TransLineMethod,max([Notes,Ref]));
	
	%%%%%%%%%%%% checking that the impedance and the peak frequency detection are sane
% 	figure;
% 	plot(Z(:,1),abs(Z(:,2)+1i*Z(:,3)),'b');
% 	hold on;
% 	plot(Fpeaks,6*10^7.*ones(size(Fpeaks)),'*r');
	%%%%%%%%%%%%
	
	ETDs{k}=ETDsCalc(InsNotes,Fpeaks,Fing,Notes,Ref);
	
	intonation(k)=IntonationCalc(InsNotes,ETDs{k},Threshold,MusicalMetric,max([Notes,Ref]),Fing,NoteRepartition);
end

%% VISUALIZATION
figure;
plot(xs.l{1},intonation);
xlabel('Geometrical variable (m)');
ylabel('intonation (cents)');
