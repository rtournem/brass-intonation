The Equally Tempered Deviation
=============================

The intonation of the note is assessed by the deviation of the
expected frequency position of the corresponding input impedance peak
from the actual frequency position of the input impedance peak. To
compute this deviation, the cent logarithmic unit is used because it
is standard to compare musical pitches (there are no scaling issues),
given that human perception of pitches is based on frequency ratios
(one cent corresponds to a frequency ratio equal to a hundredth of a
semitone).  To compute this deviation, a reference (tuning) note and a
reference scale (considered as the correct intonation) are necessary.
Trumpet players tune generally their instrument on the 4th regime of
the trumpet with no valve pressed (Bb4, concert pitch), so it is used
as the reference note.

The equal-tempered scale (which means that the octave is divided in 12
equal semi-tones) is chosen as the reference scale given its worldwide
use in occidental music.  While it is possible to consider customized
musical temperament for a particular trumpet player, such
consideration is beyond the scope of this toolbox.  For every note
$`i`$, the equal-tempered deviation between the frequency of the ith
note, $`F_i`$, and the reference frequency $`F_{\text{ref}}`$ is given
by

$` ETD(x,i) =
  \alpha_{ref \rightarrow i} 
  -1200\;log_2 \left (
    \frac{\overline{F_{i}}}{\overline{F_{ref}}} 
  \right), `$

where $` \alpha_{ref \rightarrow i} `$ is the difference between the
reference note $`ref`$ and the targeted note $`i`$ given the
equal-tempered scale (-500 cents for example between Bb4 and F4
between which the interval is a descending fourth). The intonation
$`J`$ for the whole bore is the average of the absolute deviation
across the ($`P-1`$) notes (note that the deviation between the
reference note and the 4th note is always equal to zero):

$`  J  =
  \frac{1}{P-1}\sum_{i \in \text{Notes}} \Big | ETD(i) \Big |. `$
