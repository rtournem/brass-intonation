function DrawFromGeom(EleLength,Radii,lIdxVar,rIdxVar)
%Author: Robin Tournemenne 19/05/17

CurPos=0;%position pointer
Pos=[];
Radius=[];
Pos2PlotVar=[];
Point2PlotVar=[];
for i=1:length(EleLength)
	PosReal(i)=CurPos;
	RadiusReal(i)=Radii{i}(1);
	Pos(end+1)=CurPos;
	Radius(end+1)=Radii{i}(1);
	CurPos=CurPos+EleLength(i);
	if length(Radii{i})==1 %cylinder
		Pos(end+1)=CurPos-eps; %we fake the cylinder profile for the drawing, the function eps is used to fake the superior limit
		Radius(end+1)=Radius(end);
	end
	if i==length(EleLength) %to get the very end of the bore
		Pos(end+1)=CurPos;
		Radius(end+1)=Radii{i}(end);
		PosReal(end+1)=CurPos;
		RadiusReal(end+1)=Radii{i}(end);
	end
	%%%%%%%%%%%%%%%%%% to plot the variables (comment if not needed)
	if exist('lIdxVar','var') & find(i==lIdxVar)
		if length(Radii{i})==1 %cylinder
			Pos2PlotVar=[Pos2PlotVar; Pos(end-1:end)];
		else
			Pos2PlotVar=[Pos2PlotVar; Pos(end), CurPos];
		end
	elseif exist('rIdxVar','var') & find(i==rIdxVar)
		Point2PlotVar=[Point2PlotVar; PosReal(i), RadiusReal(i)];
	end
	%%%%%%%%%%%%%%%%
	
end
Pos2Plot=0:0.000001:CurPos;
Radius2Plot=interp1(Pos,Radius,Pos2Plot);
figure;
plot(Pos2Plot,Radius2Plot,'b');
hold on;
plot(PosReal,RadiusReal,'.r','MarkerSize',5);
axis equal

%%%%%%%%%%%%%%%%%%%%% to plot the variables (comment if not needed)
for i=1:size(Pos2PlotVar,1)
	k2plot=find(Pos2PlotVar(i,1)<Pos2Plot & Pos2Plot<Pos2PlotVar(i,2));
	plot(Pos2Plot(k2plot),Radius2Plot(k2plot),'g','linewidth',2);
	llegend{i}=['length variable #' num2str(i)];
end
for i=1:size(Point2PlotVar,1)
	plot(Point2PlotVar(:,1),Point2PlotVar(:,2),'dg','MarkerSize',6,'markerfacecolor','g');
	rlegend{i}=['radius variable #' num2str(i)];
end
%%%%%%%%%%%%%%%%%%%%%
if exist('llegend','var') && exist('rlegend','var')
	s={'Bore','Control points',llegend{:},rlegend{:}};
elseif ~exist('llegend','var') && exist('rlegend','var')
	s={'Bore','Control points',rlegend{:}};
elseif ~exist('rlegend','var') && exist('llegend','var')
	s={'Bore','Control points',llegend{:}};
else
	s={'Bore','Control points'};
end
legend(s,'FontSize',12,'Fontweight','bold','Location','North');

%the symmetric part
plot(Pos2Plot,-Radius2Plot,'b');
plot(PosReal,-RadiusReal,'.r','MarkerSize',5);
for i=1:size(Pos2PlotVar,1)
	k2plot=find(Pos2PlotVar(i,1)<Pos2Plot & Pos2Plot<Pos2PlotVar(i,2));
	plot(Pos2Plot(k2plot),-Radius2Plot(k2plot),'g','linewidth',2);
end
for i=1:size(Point2PlotVar,1)
	plot(Point2PlotVar(:,1),-Point2PlotVar(:,2),'dg','MarkerSize',6,'markerfacecolor','g');
end
end