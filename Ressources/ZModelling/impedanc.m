function y=impedanc(M,Z)
%Author: Jean-Pierre Dalmont after 1990

N=length(M)/4;
if Z==-ones(size(Z))
  y=M(1:N)./M(2*N+1:3*N);
else
  y=(M(1:N).*Z+M(N+1:2*N))./(M(2*N+1:3*N).*Z+M(3*N+1:4*N));
end
