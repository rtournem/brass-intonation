function Zr=zrayon_RT(k, r, ro, Co)
%radiation function coming from the Rabiner book

omega = k * Co;
s = pi*r*r;
%Rabiner Digital Processing of Speech Signals (p71)
alpha = 3 * Co * pi / (8 * r);
beta = 9 * pi^2 / 128;
Zc = ro * Co / s;
Zr = 1i .* omega ./ (alpha + 1i .* omega .* beta) * Zc;

end
