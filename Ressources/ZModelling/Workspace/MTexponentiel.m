function matrice = MTexponentiel(L, R1, R2,k)
%Author: Jean-Pierre Dalmont after 1990

% Donne la matrice de transfert d'un pavillon exponentiel sans pertes.
% MT = MTexponentiel(L,R1,R2) renvoie un vecteur de longueur quatre fois 
%       plus grande que le vecteur k (global), puisqu'il contient � la 
%       suite les 4 �l�ments A B C D de la matrice.

S1 = pi*R1*R1; S2 = pi*R2*R2;
m = log(S1/S2)/(2*L);
k1 = sqrt(k.^2 - m^2);
Zc1 = k1./k;

a = cos(k1*L) - sin(k1*L)./(k1/m);
b = 1i*Zc1.*sin((k1*L).*(1 + (k1./m).^-2));
c = 1i*sin(k1*L)./Zc1;
d = cos(k1*L) + sin(k1*L)./(k1/m);

matrice = [a b c d];