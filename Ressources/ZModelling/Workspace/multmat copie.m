function y=multmat(matrice1,matrice2)
%Author: Jean-Pierre Dalmont after 1990

%
%MULTMAT	multiplication de deux matrices 2*2 �crites en ligne;
%		C=MULTMAT(M1,M2) renvoie un vecteur de m�me longueur
% 		que M1 et M2 r�sultat de la multiplication des 2 matrices
%	 	
% 		Voir �galement CYLSP, CYLINDRE, CONICITE, SELF, CONESP,VOLUME
% 		ainsi que INVMAT, IMPEDANC.
%Ceci EST EXTREMEMENT OPTIMIS�
N=length(matrice1)/4;
% A1=matrice1(1:N);
% B1=matrice1(N+1:2*N);
% C1=matrice1(2*N+1:3*N);
% D1=matrice1(3*N+1:4*N);
% A2=matrice2(1:N);
% B2=matrice2(N+1:2*N);
% C2=matrice2(2*N+1:3*N);
% D2=matrice2(3*N+1:4*N);
% A=A1.*A2+B1.*C2;
% B=A1.*B2+B1.*D2;
% C=C1.*A2+D1.*C2;
% D=C1.*B2+D1.*D2;
% y=[A B C D];
A=matrice1(1:N).*matrice2(1:N)+matrice1(N+1:2*N).*matrice2(2*N+1:3*N);
B=matrice1(1:N).*matrice2(N+1:2*N)+matrice1(N+1:2*N).*matrice2(3*N+1:4*N);
C=matrice1(2*N+1:3*N).*matrice2(1:N)+matrice1(3*N+1:4*N).*matrice2(2*N+1:3*N);
D=matrice1(2*N+1:3*N).*matrice2(N+1:2*N)+matrice1(3*N+1:4*N).*matrice2(3*N+1:4*N);
% y=[matrice1(1:N).*matrice2(1:N)+matrice1(N+1:2*N).*matrice2(2*N+1:3*N) matrice1(1:N).*matrice2(N+1:2*N)+matrice1(N+1:2*N).*matrice2(3*N+1:4*N) matrice1(2*N+1:3*N).*matrice2(1:N)+matrice1(3*N+1:4*N).*matrice2(2*N+1:3*N) matrice1(2*N+1:3*N).*matrice2(N+1:2*N)+matrice1(3*N+1:4*N).*matrice2(3*N+1:4*N)];
y=[A B C D];
end