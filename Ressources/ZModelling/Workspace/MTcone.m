function matrice = MTcone(L,R1,R2,n,k)
%Author: Jean-Pierre Dalmont after 1990

%CONE   matrice de transfert d'un cone de longueur L, de rayon d'entr�e R1
%       et de rayon de sortie R2
%       Le cone est d�coup� en N tron�ons pour une meilleure prise en
%       compte des pertes viscothermiques.
%       C = CONE(L,R1,R2) renvoie un vecteur de longueur quatre fois plus
%       grande que le vecteur k (global), puisqu'il contient � la suite les
%       4 �l�ments A B C D de la matrice.

if nargin<4, n=1; end % si aucun param rentr� pour le nb N de tron�ons, N=1


if R2<R1
% 	On recr�e le cas divergent:
C=R1;
R1=R2;
R2=C;
type='conv';
end

%AVANT MODIF
lcoeff = R2/R1;
S1 = pi*R1^2; S2 = pi*R2^2; % Calcul de la surface d'entr�e S1, sortie S2
l = L/(lcoeff - 1); % longueur tronqu�e (th. de Thal�s)
Ltot = l + L; % longueur du cone non tronqu�


% D�coupage du cone en n tron�ons coniques pour tenir compte des effets
% viscothermiques.
Ri0 = R1; % initialisation du rayon
ln = L/n; % longueur d'un tron�on de cone
for ii = 1:n
	Ri = (l + 1i*ln)*R1/l; % Ri varie de R1 � R2
	matrice1 = typ_cylindre(ln,sqrt(Ri0*Ri),k);
	if ii == 1
		matrice = matrice1;
	else
		matrice = multmat(matrice, matrice1);
	end
	Ri0 = Ri;
end

% Application de la conicit�
matrice = multmat(conicite(l,k), matrice);
matrice = multmat(matrice, conicite(-Ltot,k));

% "dimensionnement" de la matrice obtenue : on ne travaille plus avec des
% vitesse mais des d�bits
N = length(matrice)/4;
a = matrice(1:N)*sqrt(S2/S1);
b = matrice(N+1:2*N)/sqrt(S1*S2);
c = matrice(2*N+1:3*N)*sqrt(S1*S2);
d = matrice(3*N+1:4*N)*sqrt(S1/S2);
matrice = [a b c d];

if exist('type','var')
	MatTransdiv(1,1,:)=a;
	MatTransdiv(1,2,:)=b;
	MatTransdiv(2,1,:)=c;
	MatTransdiv(2,2,:)=d;
% % 		changement signe b et c
% 	MatTransdiv(1,2,:)=-MatTransdiv(1,2,:);
% 	MatTransdiv(2,1,:)=-MatTransdiv(2,1,:);
	for i=1:N
		invMatdiv(:,:,i)=inv(MatTransdiv(:,:,i));
	end
	%changement signe b et c
	invMatdiv(1,2,:)=-invMatdiv(1,2,:);
	invMatdiv(2,1,:)=-invMatdiv(2,1,:);
	matrice=[squeeze(invMatdiv(1,1,:)).',squeeze(invMatdiv(1,2,:)).',squeeze(invMatdiv(2,1,:)).',squeeze(invMatdiv(2,2,:)).'];
end


%--------------------- Matrice de conicite -------------------------------%
function matrice = conicite(l,k)
%CONICITE matrice de conicit� (sans pertes) pour une distance � l'origine l
%		avec k le nombre d'onde	
%		C=CONICITE(K,L) renvoie un vecteur de longueur
% 		quatre fois plus grande que le vecteur k puisque qu'il
%	 	contient � la suite les 4 �l�ments  A B C D de la matrice.
% 
% 		Voir �galement CYLSP, CYLINDRE, VOLUME, SELF, CONESP
% 		ainsi que MULTMAT, INVMAT, IMPEDANC.
a = ones(size(k));
c = 1./(1i*k*l);
b = zeros(size(k));
d = a;
matrice=[a b c d];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%---------- Prise en consid�ration des pertes viscothermiques ----------%
function matrice = typ_cylindre(L,R,k)
% Cr�ation d'une matrice de type matrice de transfert pour un cylindre avec
% un nombre d'onde complexe pour tenir compte des effets viscothermiques
global lt lv gamma  
% rv = R*sqrt(k/lv); rt = R*sqrt(k/lt);
% Zv = 1i*k.*(1+sqrt(2)./rv*(1-1i)-3*1i./rv.^2); % Au deuxi�me ordre
% Yt = 1i*k.*(1+(gamma-1)*(sqrt(2)*(1-1i)./rt + 1i./rt.^2)); % Au deuxi�me ordre
% Zv = 1i*k.*(1+sqrt(2)./(R*sqrt(k/lv))*(1-1i)-3*1i./(R*sqrt(k/lv)).^2); % Au deuxi�me ordre
% Yt = 1i*k.*(1+(gamma-1)*(sqrt(2)*(1-1i)./(R*sqrt(k/lt)) + 1i./(R*sqrt(k/lt)).^2)); % Au deuxi�me ordre
% G=sqrt(Zv.*Yt); % ou nb d'onde complexe : k = -j*sqrt(Zv.*Yt)
G=sqrt((1i*k.*(1+sqrt(2)./(R*sqrt(k/lv))*(1-1i)-3*1i./(R*sqrt(k/lv)).^2)).*(1i*k.*(1+(gamma-1)*(sqrt(2)*(1-1i)./(R*sqrt(k/lt)) + 1i./(R*sqrt(k/lt)).^2))));
% si convention nb d'onde complexe : cos(k*L)
matrice=[cosh(G*L) sinh(G*L) sinh(G*L) cosh(G*L)];
% matrice = [cosh(sqrt((1i*k.*(1+sqrt(2)./(R*sqrt(k/lv))*(1-1i)-3*1i./(R*sqrt(k/lv)).^2)).*(1i*k.*(1+(gamma-1)*(sqrt(2)*(1-1i)./(R*sqrt(k/lt)) + 1i./(R*sqrt(k/lt)).^2))))*L) sinh(sqrt((1i*k.*(1+sqrt(2)./(R*sqrt(k/lv))*(1-1i)-3*1i./(R*sqrt(k/lv)).^2)).*(1i*k.*(1+(gamma-1)*(sqrt(2)*(1-1i)./(R*sqrt(k/lt)) + 1i./(R*sqrt(k/lt)).^2))))*L) sinh(sqrt((1i*k.*(1+sqrt(2)./(R*sqrt(k/lv))*(1-1i)-3*1i./(R*sqrt(k/lv)).^2)).*(1i*k.*(1+(gamma-1)*(sqrt(2)*(1-1i)./(R*sqrt(k/lt)) + 1i./(R*sqrt(k/lt)).^2))))*L) cosh(sqrt((1i*k.*(1+sqrt(2)./(R*sqrt(k/lv))*(1-1i)-3*1i./(R*sqrt(k/lv)).^2)).*(1i*k.*(1+(gamma-1)*(sqrt(2)*(1-1i)./(R*sqrt(k/lt)) + 1i./(R*sqrt(k/lt)).^2))))*L)]; % convention ro*Co = 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%