function [ze, f, instru, Mguide, Zr] = MT_Z(instru,f,Co,ro,Ta,param_visc,show,Savefold)
%Author: Jean-Pierre Dalmont after 1990

% Calcul la matrice de transfert du quadripole de l'instru, calcul
% l'imp�dance d'entr�e de l'instru et trace l'imp�dance.
% param_visc est le parametre permettant de prendre en compte la viscosit�
% thermique : nombre de tron�ons des cones pour le calcul des pertes. Par
% d�faut, param_visc = 10 (d�coupage en 10 portions)
% show = false, ou rien, on ne trace pas, sinon, on trace
k=(2*pi/Co)*f;

if nargin < 5, show = false; end
if nargin < 4, param_visc = 10; end

Ltot = 0;
global lv lt gamma %to improve speed.
muu = 1.708e-5*(1+.0029*Ta);
lv = muu/(ro*Co);
lt = lv/.843^2;
gamma = 1.402;
% CALCUL DES MATRICES DE Transmission
for ii = 1:length(instru)
	switch lower(instru(ii).type)
		case 'cone'
			if ~isfield(instru(ii),'TransMat') || isempty(instru(ii).TransMat)% which is in fact the fixed transmission matrix
				if instru(ii).rayon(1)==instru(ii).rayon(2) %it is in fact a cylinder, then we avoid an error
					Mguide2 = MTcylindre(instru(ii).longueur,...
						instru(ii).rayon(1),k,Ta,ro,Co);
				else
					Mguide2 = MTcone(instru(ii).longueur,...
						instru(ii).rayon(1), instru(ii).rayon(2),...
						param_visc,k);
				end
				instru(ii).TransMat=Mguide2;
			else
				Mguide2=instru(ii).TransMat;
			end
		case 'cylindre'
			if ~isfield(instru(ii),'TransMat') || isempty(instru(ii).TransMat)
				Mguide2 = MTcylindre(instru(ii).longueur,...
					instru(ii).rayon,k,Ta,ro,Co);
				instru(ii).TransMat=Mguide2;
			else
				Mguide2=instru(ii).TransMat;
			end
		case 'exponentiel', Mguide2 = MTexponentiel(instru(ii).longueur,...
                instru(ii).rayon(1), instru(ii).rayon(2),k);
		otherwise
			error('�l�ment non g�r�');
    end
    Ltot = Ltot + instru(ii).longueur;
    if ii == 1
        Mguide = Mguide2;
    else
        Mguide = multmat(Mguide,Mguide2);
    end
end

% Imp�dance de rayonnement
% Le dernier param�tre de la fonction zrayon d�finit
% la nature de Zr imp�dance de rayonnement :
%     0 (Zr=0) ou 1 (rayonnement sans �cran, approx. Norris/Sheng)
%      ou 3 (Zr=ro.c/S)
Rmax = instru(end).rayon(end);
Rmin = instru(1).rayon(1);
Zr=zrayon(k,Rmax,1);

% Calcul imp�dance d'entr�e
% imp�dance d'entr�e avec dimension (pression/d�bit)
% en supposant ro.c=1 (convention "fonctions JPD")
zejpd=impedanc(Mguide,Zr);
% zead: imp�dance d'entr�e avec dimension (pression/d�bit)
zead=zejpd*(ro*Co);
% imp�dance d'entr�e sans dimension (imp�dance r�suite)
zs=ro*Co/(pi*Rmin^2);     %imp�dance it�rative (pression-d�bit)
zesd=zead/zs;            %imp�dance d'entr�e sans dimension

% TRACE de l'imp�dance d'entr�e
ze=zesd;                 %imp�dance d'entr�e retenue pour le trac�
% ze = zead;

if show
    ff=figure;
    subplot(321); plot(f,abs(ze),'k'); axis; title('Module Ze');
    subplot(323); plot(f,angle(ze),'k'); axis; title('Phase Ze');
    subplot(322); plot(f,real(ze),'k'); axis; title('Real(Ze)');
    subplot(324); plot(f,imag(ze),'k'); axis; title('Imag(Ze)');
    % trac� de la perce
    x = 0:Ltot/10000:Ltot; p = perce(x,instru);
	%points donn�s:
	for i=1:length(instru)
		if i>1
			xsamp(i)=xsamp(i-1)+instru(i-1).longueur;
		else
			xsamp(1)=0;
		end
		ysamp(i)=instru(i).rayon(1);
	end
    subplot(313); plot(x,p,'-k',x,-p,'-k',xsamp,ysamp,'*m'); title('Perce')

	gg=figure;
    subplot(211), semilogy(f,abs(ze),'k'); axis; grid; title('Ze[dB]');
    subplot(212), plot(x,p,'black',x,-p,'black'); title('Resonator');xlabel('length');ylabel('radius');
	if Savefold
		savefig(ff,[Savefold 'Perce_' num2str(length(instru)) 'elts_' clockFormat]);
		savefig(gg,[Savefold 'Imped_' num2str(length(instru)) 'elts_' clockFormat]);
	end
end