function y=impedanc(M,Z)
%Author: Jean-Pierre Dalmont after 1990

%IMPEDANC	calcule l'impedance d'entr�e associ�e � une matrice de 
%           transfert M �crite en ligne et ferm�e par une impedance Z;	
%		C=IMPEDANC(M,Z) renvoie un vecteur de m�me longueur
% 		que Z et de longueur 4 fois plus petite que M
%               r�sultat de l'inversion de la matrice.
%               Pour une impedance infinie le vecteur Z sera Z=-ONES(size(M)/4)
%	 	
% 		Voir �galement CYLSP, CYLINDRE, CONICITE, SELF, CONESP,VOLUME
% 		ainsi que MULTMAT, INVMAT.
N=length(M)/4;
if Z==-ones(size(Z))
  y=M(1:N)./M(2*N+1:3*N);
else
  y=(M(1:N).*Z+M(N+1:2*N))./(M(2*N+1:3*N).*Z+M(3*N+1:4*N));
end