function y=zrayon(k,r,o)
%Author: Jean-Pierre Dalmont after 1990

%ZRAYON 	imp�dance de rayonnement  d'un cylindre de rayon int�rieur r
%		avec k le nombre d'onde.
%		Z=ZRAYON(K,R,S,O) renvoie un vecteur de longueur de m�me longueur
%		que le vecteur k.
%		O est un param�tre d�finissant le type d'imp�dance :
%		O=0 imp�dance nulle
%		O=1 imp�dance de rayonnement sans �cran
%		O=2 imp�dance de rayonnement avec �cran infini
%               O=3 impedance adapt�e (roce/s)
%		Voir �galement CYLSP, CONESP, VOLUME, SELF, CONICITE
%		ainsi que MULTMAT, INVMAT, IMPEDANC.

s = pi*r*r;
if o==0
	y=zeros(size(k));
elseif o==3
	y=1/s*ones(size(k));
elseif o==2
	aux=.77*k*r;
	deltal=0.82159*(1+aux)./(1+aux+aux.^2)*r;
	alfa=.323;
	beta=-.077;
	R=(1+alfa*k*r+beta*(k*r).^2)./(1+alfa*k*r+(.5+beta)*(k*r).^2);
	R=R.*(sign(R)+1+1e-10)/2;
	y=j/s*tan(k.*deltal+1/2*j*log(R));
elseif o==1
	deltal=0.6133*(1+(0.21*k*r).^2)./(1+(.436*k*r).^2)*r;
	alfa=.2;
	beta=-.084;
	R=(1+alfa*k*r+beta*(k*r).^2)./(1+alfa*k*r+(1+beta)*(k*r).^2);
	y=j/s*tan(k.*deltal+1/2*j*log(R));
end;