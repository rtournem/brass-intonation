function matrice=MTcylindre(l,r,k,Tc,ro,Co)
%Author: Jean-Pierre Dalmont after 1990

%CYLINDRE	matrice de tranfert avec pertes d'un cylindre
%		de longueur l, de rayon r et de section s � la temp�rature t
%		avec k le nombre d'onde.
%		C=CYLINDRE(K,L,R,S,T) renvoie un vecteur de longueur
%		quatre fois plus grande que le vecteur k puisque qu'il
%		contient � la suite les 4 �l�ments  A B C D de la matrice.%
% 		Attention: on a pos� roce=1
%
%		Voir �galement CYLSP, CONESP, VOLUME, SELF, CONICITE
%		ainsi que MULTMAT, INVMAT, IMPEDANC.

s = pi*r*r;
muu = 1.708e-5*(1 + .0029*Tc);
lv = muu/(ro*Co); lt = lv/.843^2; gamma = 1.402;
rv = r*sqrt(k/lv); rt = r*sqrt(k/lt);%Robin: rv est juste, je suppose que rt aussi

%ajout RObin th�se de Allistair
% zc=(ro*Co)/s;
% k2=k.*(1.045./rv+1i*(1+1.045./rv));
% zc2=zc.*((1+0.369./rv)-1i*0.369./rv);
% a=cosh(k2*l); b=zc2.*sinh(k2*l);
% c=(1./zc2).*sinh(k2*l); d=a;

%retour TOOLBOX
Zv = 1i*k.*(1 + sqrt(2)./rv*(1-1i) - 3*1i./rv.^2);
Yt = 1i*k.*(1 + (gamma-1)*(sqrt(2)*(1-1i)./rt + 1i./rt.^2));
% Zc = ro*Co/s;
G = sqrt(Zv.*Yt); 
a = cosh(G*l); b = sinh(G*l)/s;
c = sinh(G*l)*s;     d = a;
matrice = [a, b, c, d];
