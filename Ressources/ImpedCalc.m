function [Z,Fpeaks,peaksvl]=ImpedCalc(Geometry,xs,k,TransLineMethod,RegMax)
%Author: Robin Tournemenne 19/05/17

%modification of the geometry according to the variables
if isfield(xs,'l')
	for i=1:length(xs.l)
		Geometry.l(xs.lpos(i))=xs.l{i}(k);
	end
end
if isfield(xs,'r')
	for i=1:length(xs.r)
		if xs.rpos(i)-1>0 & length(Geometry.r{xs.rpos(i)-1})>1 & Geometry.r{xs.rpos(i)-1}(2)==Geometry.r{xs.rpos(i)}(1)
			Geometry.r{xs.rpos(i)-1}(2)=xs.r{i}(k); %in the case we have continuous cones
		end
		Geometry.r{xs.rpos(i)}(1)=xs.r{i}(k);
	end
end
%choice of the calculation method
%-plane wave hypothesis
%-multimodal hypothesis
if strcmp(TransLineMethod,'1DTL')
	%data processing in order to use the JeanPierre Dalmont Toolbox
	Geom1D(1).Nb_elts=length(Geometry.l);
	for i=1:length(Geometry.l)
		Geom1D(i).longueur=Geometry.l(i);
		Geom1D(i).rayon=Geometry.r{i};
		if length(Geometry.r{i})>1
			Geom1D(i).type='cone';
		else
			Geom1D(i).type='cylindre';
		end
	end
	Fmax = 5500; %highest frequency where Z will be defined (in practice after 5500 Hz there are no peaks anymore)
	N = 2^12;     %number of frequency samples (to adjust in order to get a not too long impedance calculation). 2^10 points doesn't work (for 5500Hz max), 2^11 is not precise, 2^12 is good, and then the higher the better for peak frequency position detection
	f = (1:N)*Fmax/N;
	
	param_visc=10;%Z creation parameter
	show=0;      %to see graphs
	To=273.16;
	Ta=27;  %temperature of the air in the bore (to modify if needed)
	T=To+Ta; % temperature avec Ta=masque(3,1);
	Co=331.45*sqrt(T/To); % sound speed
	ro=1.2929*(To/T); % air density
	
	[Z,f,~]=MT_Z_RT(Geom1D,f,Co,ro,Ta,param_visc,show,0);
	Z=[f',real(Z'),imag(Z')];
	
	Z(:,2)=Z(:,2).*(ro*Co/(pi*Geometry.r{1}(1)^2));
	Z(:,3)=Z(:,3).*(ro*Co/(pi*Geometry.r{1}(1)^2));
	
elseif strcmp(TransLineMethod,'NDTL')
	%file not distrributed look at artsimtool.
	%data processing in order to use the Braden/Kausel Toolbox
	%Je pense juste le save de data
	ElementsLength=Geometry.l;
	ElementsRadii=Geometry.r;
	save('Ressources/Bore.mat','ElementsLength','ElementsRadii');
	
	%defining the py file location
	BradenPyFilePath=fileparts('Ressources/MultiModal.py');
	path(path,BradenPyFilePath);%if you don't add the folder in the path matlab won't find the file and will not provide the absolute path
	fullPathAndPath = which('Ressources/MultiModal.py');
	
	%calculating
	system(['python ' fullPathAndPath]);
	Z=load([BradenPyFilePath '/Z.csv']);
	system(['rm -f ' BradenPyFilePath '/Z.csv' ' 2>/dev/null']);
end

%extracting the peaks frequency position.
[Fpeaks,~,~,peaksvl] = PeaksHolesSearch(Z(:,1),Z(:,2)+1i*Z(:,3),RegMax);
end
