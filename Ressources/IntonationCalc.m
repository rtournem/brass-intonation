function intonation=IntonationCalc(InsNotes,ETDs,Threshold,MusicalMetric,RegMax,Fing,NoteRepartition)
%Author: Robin Tournemenne 19/05/17

if MusicalMetric
	ETDsCoef=ETDs2ETDsCoef(InsNotes,RegMax,Fing,ETDs,NoteRepartition);
	%the intonationonicity descriptor, the lower the better
	intonation=sum(abs(ETDsCoef(:)))/sum(ETDs(:)~=0);
	if ~isempty(Threshold) && (logical(sum(ETDsCoef(:)<Threshold(1))) || logical(sum(ETDsCoef(:)>Threshold(2))) )
		intonation=NaN;
	end
else
	intonation=sum(abs(ETDs(:)))/sum(ETDs(:)~=0);
	if ~isempty(Threshold) && (logical(sum(ETDs(:)<Threshold(1))) || logical(sum(ETDs(:)>Threshold(2))) )
		intonation=NaN;
	end
end
end

function ETDsCoef=ETDs2ETDsCoef(InsNotes,RegMax,Fing,ETDs,NoteRepartition)

names={'C' 'Db' 'D' 'Eb' 'E' 'F' 'Gb' 'G' 'Ab' 'A' 'Bb' 'B'};
pitch={'-1' '0' '1' '2' '3' '4' '5' '6' '7' '8' '9'};

load(NoteRepartition);

ETDsCoef=zeros(size(ETDs));
%defining coefs that will make sense in the comparison with the classic
%estimator
for j=1:length(Fing)
Notes=InsNotes{Fing(j)};
Coef=zeros(1,RegMax);
for i=1:RegMax
	NameReg= strcmp(Notes{i}(1:end-1),names);
	PitchesReg= strcmp(Notes{i}(end),pitch);
	Coef(j,i)=Durat_note(NameReg,PitchesReg);
end
end

%balancing the total amount of coef to be able to compare raw and weighted descriptors
Coef=sum(ETDs(:)~=0).*Coef./sum(Coef(:));

ETDsCoef=ETDs.*Coef;
end