function [peaksfr,peaksQ,idxshigh,peaksvl,lowsfr,lowsvl,idxslow]=PeaksHolesSearch(freq_Z,Z,MaxPeaks)
%Author: Robin Tournemenne in 2015

%you enter just the freqs and value of the impedance and it returns you the peaks
%holes detection!
%MaxPeaks is the number of peak you wanna detect
peaksfr=[];
peaksvl=[];
peaksQ=[];
idxshigh=[];
idxhigh=1;
lowsfr=freq_Z(1);
lowsvl=abs(Z(1));
idxslow=1;
notmoving=0;
for i=1:length(Z)
	%recherche du maximum
	if isempty(peaksfr) || length(idxshigh)<length(idxslow)
		if abs(Z(i))>abs(Z(idxhigh))
			idxhigh=i;
			notmoving=0;
		else
			notmoving=notmoving+1;
		end
		if notmoving==15
			peaksfr=[peaksfr freq_Z(idxhigh)];
			peaksvl=[peaksvl abs(Z(idxhigh))];
			
			
			idxshigh=[idxshigh idxhigh];
			idxlow=idxhigh+1;
			notmoving=0;
		end
	end
	%puis recherche du minimum suivant
	if length(idxshigh)==length(idxslow)
		if abs(Z(i))<abs(Z(idxlow))
			idxlow=i;
			notmoving=0;
		else
			notmoving=notmoving+1;
		end
		if notmoving==15
			lowsfr=[lowsfr freq_Z(idxlow)];
			lowsvl=[lowsvl abs(Z(idxlow))];
			
			
			idxslow=[idxslow idxlow];
			idxhigh=idxlow+1;
			notmoving=0;
			if length(peaksfr)==MaxPeaks
				break;
			end
		end
	end
end


end