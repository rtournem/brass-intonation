function ETDs=ETDsCalc(InsNotes,Fpeaks,Fing,Notes,Ref)
%Author: Robin Tournemenne 19/05/17

names={'C' 'Db' 'D' 'Eb' 'E' 'F' 'Gb' 'G' 'Ab' 'A' 'Bb' 'B'};

EqTempDist=zeros(12,12);
for j=1:11
	EqTempDist=EqTempDist+diag(100*j.*ones(1,12-j),j)+diag(100*(-j).*ones(1,12-j),-j);
end

for l=Notes
	if l~=Ref
		%finding the ideal distance in cents in the well-tempered scale.
		NotesOfTheFing=InsNotes{Fing};
		
		NoteRef=NotesOfTheFing{Ref};
		OctRef=str2double(NoteRef(end));
		NoteRefIdx= strcmp(NoteRef(1:end-1),names);
		NoteCur=NotesOfTheFing{l};
		OctCur=str2double(NoteCur(end));
		NoteCurIdx= strcmp(NoteCur(1:end-1),names);
		
		Ideal=1200*(OctCur-OctRef)+EqTempDist(NoteRefIdx,NoteCurIdx);
		ETDs(l)=1200*log2(Fpeaks(l)/Fpeaks(Ref))-Ideal;

	end
end