# -*- coding: utf-8 -*-
# Author: Robin Tournemenne 19/05/17

import sys
import os
import numpy as np
import scipy.io
from sys import platform;


if platform.startswith("win") :
	os.chdir('c:\YOUR\PATH\TO\THIS\FILE')
elif platform.startswith("linux") :
	os.chdir('/YOUR/PATH/TO/THIS/FILE/')

#In order to use the multimodal input impedance calculation, you have to install the ART toolbox to produce the file libartsim.so for Linux or the file artsim.dll for Windows

#the version of the source code you should compile is located at this adress: https://sourceforge.net/p/artool/code/HEAD/tree/trunk/
#click on the button "Download Snapshot"
#extract the zip file
#Go in your terminal, cd to the subfolder cmake
#enter: cmake CMakeLists.txt -DRELEASE=ON
#enter: make
#if everything goes well you will get the files libartsim.so and art.dll in the subfolder out/release/ (otherwise try to ask for help to Clemens Geyer)

#Copy them to the folder where is situated this present file
#Copy the file artsimdefs.py located in the ART/examples/Python subfolder to the location of the present file and modify line 7 and 9 to point to the right path.
#In the end you should have the three files MultiModal.py, artsymdefs.py and libartsim.so in the same folder Ressources.

#It is all you need to use the multimodal input impedance calculation.
#YOU ALSO NEED TO ADJUST THE NUMBER OF MODE YOU NEED (line 94)

#Unfortunately compiling the ART toolbox can be tricky and getting the file libartsim.so or artsim.dll can be hard.
#I tried for 2 weeks to compile it on MAC OS X but I couldn't resolve all the 32bit kernel problems...

from artsimdefs import *;
from math import sqrt;

Fmax=5500;

MatFile=scipy.io.loadmat('Bore.mat');
LengthArray=MatFile['ElementsLength'];

LengthListUnclean=LengthArray[0].tolist();
LengthList=[];
for val in LengthListUnclean:
	LengthList.extend([val*100]);#conversion from m to cm because of ART toolbox

print '------------------'
print LengthList
print '------------------'

RadiiArray=MatFile['ElementsRadii'];

RadiiListUnclean=RadiiArray[0].tolist();

RadiiList=[]
for val in RadiiListUnclean:
	if len(val[0].tolist())>1:
		radiicone=val.tolist()[0];
		RadiiList.extend([[radiicone[0]*100, radiicone[1]*100]]);
	else:
		RadiiList.extend([val[0].tolist()[0]*100]);

print '------------------'
print RadiiList
print '------------------'

# init simulation
pSim = ARTRootObject();

mySim = ARTCreateSimulator("MySimulator", "FrequencyDomain", "MultiModal");

InstruType=[]

for i in range(len(LengthList)-1):
	if isinstance(RadiiList[i], list):
		InstruType.append(ARTCreateElement(mySim, "Con"+str(i), "Cone"));
		ARTSetParameter(mySim, "Con"+str(i)+".length = "+str(LengthList[i])+";");
		ARTSetParameter(mySim, "Con"+str(i)+".r1 = "+str(RadiiList[i][0])+"; "+"Con"+str(i)+\
		".r2 = "+str(RadiiList[i][1])+";");
	else:
		InstruType.append(ARTCreateElement(mySim, "Cyl"+str(i), "Cylinder"));
		ARTSetParameter(mySim, "Cyl"+str(i)+".r = "+str(RadiiList[i])+";"+"Cyl"+str(i)+".length = "+str(LengthList[i]));

# Create circuit and add elements to circuit
myIns = ARTCreateCircuit(mySim, "MyInstrument");

for i in range(0,len(LengthList)-1):
	ARTAppendReference(myIns, InstruType[i]);

# Set simulation properties
ARTSetFrequencyRange(mySim, 10, Fmax, 1);

# ------------------ NUMBER OF MODES DESIRED ---------------------
ARTSetNModes(mySim, 2);
# ----------------------------------------------------------------

print "Calculating...",
myImpCurve = ARTGetValue(ARTInputImpedance(myIns));
print "Done.\n";

fid=open('Z.csv','w');
for i in range(0, ARTGetLength(myImpCurve)) :
	# get data structure
	tri = ARTGetTriple(myImpCurve, i);
	# compute magnitude
	mag = sqrt(tri.re*tri.re + tri.im*tri.im);
	fid.write(str(tri.f) +","+ str(tri.re) +","+str(tri.im) + "\n");

fid.close()

ARTDestroyCircuit(mySim, myIns);

for i in range(len(LengthList)-1):
	ARTDestroyElement(mySim, InstruType[i]);

ARTDestroySimulator(mySim);
ARTRootDestroy();
