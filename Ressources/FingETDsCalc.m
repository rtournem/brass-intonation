function ETDs=FingETDsCalc(InsNotes,Fpeaks,Fing,Notes,Ref,ETDs)
%Author: Robin Tournemenne 19/05/17

names={'C' 'Db' 'D' 'Eb' 'E' 'F' 'Gb' 'G' 'Ab' 'A' 'Bb' 'B'};

EqTempDist=zeros(12,12);
for j=1:11
	EqTempDist=EqTempDist+diag(100*j.*ones(1,12-j),j)+diag(100*(-j).*ones(1,12-j),-j);
end

%finding the ideal distance in cents in the well-tempered scale.
NotesOfTheFing=InsNotes{1};
NoteRef=NotesOfTheFing{Ref};
OctRef=str2double(NoteRef(end));
NoteRefIdx= strcmp(NoteRef(1:end-1),names);

NotesOfTheFing=InsNotes{Fing};
NoteCur=NotesOfTheFing{Ref};
OctCur=str2double(NoteCur(end));
NoteCurIdx= strcmp(NoteCur(1:end-1),names);

Ideal=1200*(OctCur-OctRef)+EqTempDist(NoteRefIdx,NoteCurIdx);
distFing=1200*log2(Fpeaks(Fing,Ref)/Fpeaks(1,Ref))-Ideal;

ETDs([Notes,Ref])=ETDs([Notes,Ref])+distFing;

end