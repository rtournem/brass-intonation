%Author: Robin Tournemenne 19/05/17

%% HEADING
clear variables global
close all
path(path,genpath('Ressources'));

%% PROBLEM DEFINITION
%The instrument tessiture, this stands for the Bb trumpet only!
InsNotes = {{'E2','Bb3','F4','Bb4','D5','F5','Ab5','Bb5'};
	{'D2','Ab3','Eb4','Ab4','C5','Eb5','Gb5','Ab5'};
	{'Eb2','A3','E4','A4','Db5','E5','G5','A5'};
	{'C2','Gb3','Db4','Gb4','Bb4','Db5','E5','Gb5'}};
%The instrument note recurrence (for the MusicalMetric), this stands for the Bb trumpet only!
NoteRepartition='Ressources/NoteRepartition.mat';

%datafile:
DataFile='Ressources/Impedance.txt';

%% PROGRAM PARAMETERs
Fing=1; %the settings are for a trumpet. If you want to modify the instrument you have to modify valueSet in ETDsCalc.m
%1=D0 (no valve pressed), 2=D1 (first valve pressed), 3=D2 (second valve pressed), 4=D23
Notes=[2,3,5]; %don't put the reference note
Ref=4;
TransLineMethod='1DTL'; %'1DTL'  'NDTL'
Threshold=[];%Threshold=[-20,+50]; %empty vector to avoid using it %these thresholds corresponds to acceptance limit for intonationonicity (in practise it is hard for a musician to increase the pitch of a note while it is easy to lower it)
MusicalMetric=0; %take into account the recurrence of a note in the repertoire of the instrument.

Z=load(DataFile);
To=273.16;
Ta=27;  %temperature of the air in the bore (to modify if needed)
T=To+Ta; % temperature avec Ta=masque(3,1);
Co=331.45*sqrt(T/To); % sound speed
ro=1.2929*(To/T); % air density

Z(:,2)=Z(:,2).*(ro*Co/(pi*0.008^2));
Z(:,3)=Z(:,3).*(ro*Co/(pi*0.008^2));

try
	Fpeaks=circle_fitting(max([Notes,Ref]),Z(:,1),Z(:,2)+1i*Z(:,3),0,0);
catch
	Fpeaks=PeaksHolesSearch(Z(:,1),Z(:,2)+1i*Z(:,3),max([Notes,Ref]));
end

%%%%%%%%%%% checking that the impedance and the peak frequency detection are sane
figure;
plot(Z(:,1),abs(Z(:,2)+1i*Z(:,3)),'b');
hold on;
plot(Fpeaks,6*10^7.*ones(size(Fpeaks)),'*r');
%%%%%%%%%%%

ETDs=ETDsCalc(InsNotes,Fpeaks,Fing,Notes,Ref);

intonation=IntonationCalc(InsNotes,ETDs,Threshold,MusicalMetric,max([Notes,Ref]),Fing,NoteRepartition);

%% VISUALIZATION
disp(['the intonation is ' num2str(intonation) ' cents']);
