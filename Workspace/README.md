<img src="Ressources/ExampleWIIC.gif">

# Wind Instruments Intonation Calculator

This Matlab toolbox let you compute the intonation of brass instrument bores (geometrical internal shape) based on their input impedance.
You can compute the intonation of a given bore (known by its geometry or its input impedance) or compute the intonation of bores for which some geometrical variables vary (see the gif above showing a bore for which two geometrical parameters vary).
The intonation calculation is the average of the Equally tempered Deviation (ETD) of each note of interest given a note of reference. The ETDs are described [here](https://docs.google.com/document/d/1AcSuElGCFWsTGnwGT6AmkZgXe7dHrESHriY7D0GY03A/edit?usp=sharing). 

## Features
* Computation of the bore input impedance from the bore geometry,
* The computation of the input impedance can be made by either a standard planar wave assumption or a multimodal assumption,
* Intonation of bores having an arbitrary number of geometrical variables,
* Intonation of an instrument taking into account different fingering,
* Intonation using a musical metric based on the recurrence of each note given a large variety of scores,
* Exclusion of bores producing at least one note too much out of tune (using user defined thresholds). 

## Installation

Simply download all the files and use the functions included in the Ressources folder.


In case you want to use the multimodal input impedance calculation feature, you will need to adjust paths in the MultiModal.py file.
Besides, although all necessary files are already in the Ressources folder, the library libartsim.so (for Linux) may not work since you may be using another Linux distribution, Windows or MAC OS X. The library libartsim.so for Linux or libartsim.dll for Windows is produced while compiling the ART toolbox. We now detail the required steps to install this toolbox:

* go to [https://sourceforge.net/p/artool/code/HEAD/tree/trunk/](https://sourceforge.net/p/artool/code/HEAD/tree/trunk/)
* click on "Download Snapshot"
* unzip the file where you like
* open a terminal, cd to the subfolder cmake/
* enter: cmake CMakeLists.txt -DRELEASE=ON
* enter: make
* hope
* copy the libartsim.so (or libartsim.dll) file located in the subfolder out/release to the Ressources folder.

It can be tricky to install the toolbox, don't hesitate to ask for help to the austrian team developping it!
Unfortunately, MAC OS X users cannot use the multimodal input impedance calculation feature...


## Usage

### Get Started

In order to test and use the toolbox you can directly launch the scripts in the root folder.
These scripts are dealing with the Bb Trumpet:

* MainIntoSimpleImped.m: calculation of the intonation of one specific bore
* MainInto1D.m: calculation of the intonation of a bore depending on 1 geometrical variable
* MainInto2D.m: calculation of the intonation of a bore depending on 2 geometrical variables
* MainIntoND.m: calculation of the intonation of a bore depending on 4 geometrical variables
* MainInto1D_MultiFing.m: calculation of the intonation of a complete instrument depending on one geometrical variable over 2 fingering

### A Single Bore

```Matlab
%the playable notes of the instrument (here the Bb trumpet)
InsNotes = {{'E2','Bb3','F4','Bb4','D5','F5','Ab5','Bb5'};
	{'D2','Ab3','Eb4','Ab4','C5','Eb5','Gb5','Ab5'};
	{'Eb2','A3','E4','A4','Db5','E5','G5','A5'};
	{'C2','Gb3','Db4','Gb4','Bb4','Db5','E5','Gb5'}};
Fing=1; %Which instrument fingering you want to study
Notes=[2,3,5]; %the note you are interested in for the intonation calculation
Ref=4; %the reference note
Threshold=[];%these thresholds corresponds to acceptance limit for intonation, empty vector means not used
MusicalMetric=0; %take into account the recurrence of a note in the repertoire of the instrument. 0 means that this is not taken into account

DataFile='Ressources/Impedance.txt'; %your data file containing the impedance
Z=load(DataFile); %loading the impedance
%(you could have declared a geometry and calculated afterwards its impedance)

Fpeaks=circle_fitting(max([Notes,Ref]),Z(:,1),Z(:,2)+1i*Z(:,3),0,0); %detection of the input impedance peak frequency positions

ETDs=ETDsCalc(InsNotes,Fpeaks,Fing,Notes,Ref); %calculation of the ETD (it is tha basis of the intonation calculation)

intonation=IntonationCalc(InsNotes,ETDs,Threshold,MusicalMetric); %calculation of the intonation
```

### 1 variable dependant Bores
You first have to declare a bore geometry, based on a concatenation of cylinders or cones:

```Matlab
Geometry.l=[0.005,0.08,0.055,0.055,0.055,0.055,0.495,0.02,0.019,0.03,0.035,0.03,0.029,0.03,0.031,0.03,0.061,0.03,0.031,0.032,0.03,0.025,0.036,0.03,0.034,0.029,0.028];
Geometry.r={0.008,0.001825,[4.64e-03,5e-03],[5e-03,5.5e-03],[5.5e-03,5.7e-03],[5.7e-03,5.82e-03],[5.82e-03,5.825e-03],[5.825e-03,6.125e-03],[6.125e-03,6.1e-03],[6.1e-03,5.985e-03],[5.985e-03,6.205e-03],[6.205e-03,6.325e-03],[6.325e-03,6.65e-03],[6.65e-03,6.97e-03],[6.97e-03,7.39e-03],[7.39e-03,8.45e-03],[8.45e-03,8.68e-03],[8.68e-03,9.25e-03],[9.25e-03,9.845e-03],[9.845e-03,1.0645e-02],[1.0645e-02,1.155e-02],[1.155e-02,1.275e-02],[1.275e-02,1.4375e-02],[1.4375e-02,1.67e-02],[1.67e-02,2.25e-02],[2.25e-02,3.47e-02],[3.47e-02,6.1e-02]};
```

Then supposing that your variable is a length, you have to declare the following structure:
 
```Matlab
xs.lpos=1; %index of Geometry.l that you want to study
[xs.l{1}]=ndgrid(0:0.0005:0.006);%ndgrid takes all the values you want to test for each variable and creates arrays of the corresponding dimension (1dim vector, 2dim matrix, 3dim 3dimarray, 4dim 4dimarray ...)
```

The function DrawFromGeom.m draw the bore you just constructed abstractly:


```Matlab
DrawFromGeom(Geometry.l,Geometry.r,xs.lpos,[]);
```

Finally, you loop on the values of xs.l{1}:


```Matlab
TransLineMethod='1DTL'; %variable letting you choose between the 1D or ND assumption for the calculation of the input impedance

for k=1:length(xs.l{1}(:))
	
	[Z,Fpeaks]=ImpedCalc(Geometry,xs,k,TransLineMethod,max([Notes,Ref])); %calculation of the input impedance and peaks frequency location
	
	ETDs{k}=ETDsCalc(InsNotes,Fpeaks,Fing,Notes,Ref);
	
	intonation(k)=IntonationCalc(InsNotes,ETDs{k},Threshold,MusicalMetric,max([Notes,Ref]),Fing,NoteRepartition);
end
```

### N variables dependant Bores

The only difference lies in the variable declaration, which could be for example:

```Matlab
xs.lpos=1; %index of Geometry.l that you want to study
xs.rpos=[41,42,43]; %index of Geometry.r that you want to study (it concerns the first radius if two radii are provided (cones), the radius from the previous element is adapted is needed)
[xs.l{1},xs.r{1},xs.r{2},xs.r{3}]=ndgrid(0:0.0015:0.006,0.0048:0.0002:0.0055,0.005:0.0002:0.0059,0.0053:0.0002:0.006);%ndgrid takes all the values you want to test for each variable and create arrays of the corresponding dimension (1dim vector, 2dim matrix, 3dim 3dimarray, 4dim 4dimarray ...) 
	
```

### Instrument considering several fingering

If you want to consider 2 fingerings, you now have two declare 2 geometries (a fingering is a modification of the waveguide geometry)


```Matlab
Geometry(1).l=[0.005,0.08,0.055,0.055,0.055,0.055,0.495,0.02,0.019,0.03,0.035,0.03,0.029,0.03,0.031,0.03,0.061,0.03,0.031,0.032,0.03,0.025,0.036,0.03,0.034,0.029,0.028];
Geometry(1).r={0.008,0.001825,[4.64e-03,5e-03],[5e-03,5.5e-03],[5.5e-03,5.7e-03],[5.7e-03,5.82e-03],[5.82e-03,5.825e-03],[5.825e-03,6.125e-03],[6.125e-03,6.1e-03],[6.1e-03,5.985e-03],[5.985e-03,6.205e-03],[6.205e-03,6.325e-03],[6.325e-03,6.65e-03],[6.65e-03,6.97e-03],[6.97e-03,7.39e-03],[7.39e-03,8.45e-03],[8.45e-03,8.68e-03],[8.68e-03,9.25e-03],[9.25e-03,9.845e-03],[9.845e-03,1.0645e-02],[1.0645e-02,1.155e-02],[1.155e-02,1.275e-02],[1.275e-02,1.4375e-02],[1.4375e-02,1.67e-02],[1.67e-02,2.25e-02],[2.25e-02,3.47e-02],[3.47e-02,6.1e-02]};

Geometry(2).l=Geometry(1).l;
Geometry(2).r=Geometry(1).r;
Geometry(2).l(7)=0.67; %adding length on the central cylinder of the trumpet
```
The loop differs a bit because you have to consider the deviation between the references of each fingering:

```Matlab
Fing=[1,2]; %you now have two fingerings
for k=1:length(xs.l{1}(:))
	for CurFing=1:length(Geometry)
		[Z,Fpeaks(Fing(CurFing),:)]=ImpedCalc(Geometry(CurFing),xs,k,TransLineMethod,max([Notes,Ref]));
		
		ETDs{k}(CurFing,:)=ETDsCalc(InsNotes,Fpeaks(Fing(CurFing),:),Fing(CurFing),Notes,Ref);
		
		if Fing(CurFing)~=1 %here I consider that the ref is on the fingering D0.
			ETDs{k}(CurFing,:)=FingETDsCalc(InsNotes,Fpeaks,Fing(CurFing),Notes,Ref,ETDs{k}(CurFing,:));%Adjustement of the ETDs of the current fingering according to the references distance
		end
		
	end
	intonation(k)=IntonationCalc(InsNotes,ETDs{k},Threshold,MusicalMetric,max([Notes,Ref]),Fing,NoteRepartition);
end
```
## Acknowledgment

A huge thanks to Pr. Jean-Pierre Dalmont for his 1D input impedance calculation toolbox and to Dr. Braden, Pr. Kausel ands its team for the open source ART toolbox implementing the multimodal input impedance calculation. Dr. Pauline Eveno is also warmly thanked for her peak frequency localisation based on the Nyquist space.
